import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-panel-informativo',
  templateUrl: './panel-informativo.component.html',
  styleUrls: ['./panel-informativo.component.scss']
})
export class PanelInformativoComponent implements OnInit {
  public notificacion;
  constructor(    
    private activeRoute:ActivatedRoute,
    private ruteador:Router,
    private notificaionesService: NotificacionService
    ) {
      this.busqueda(this.activeRoute.snapshot.paramMap.get('id'));
    }

  ngOnInit(): void {
  }
  busqueda(id:any)
  {
    let verNotificacion = JSON.parse(localStorage.getItem("info1"));
    let vNotificacion = JSON.parse(localStorage.getItem("info2"));
    this.notificacion = verNotificacion.find(item => item.id_noti == id);
    if(this.notificacion != null)
    {
      this.notificaionesService.eliminar_notificacion(id, verNotificacion);
      vNotificacion.push(this.notificacion);
      localStorage.setItem('info2', JSON.stringify(vNotificacion));
      this.notificaionesService.set_notificacion_vista(this.notificacion);
    }
    else{
      this.notificacion = vNotificacion.find(item => item.id_noti == id);
    }
  }
  set_notificacion_vista()
  {
    let id_nots:any; 
    console.log('-->',localStorage.getItem("id_nots"))
    id_nots = JSON.stringify(localStorage.getItem("id_nots"));
    console.log('>>>>>', id_nots,id_nots !== null);
    if(id_nots != 'null')
    {
      console.log("SI");

    }else{
      console.log("NO");
      console.log(this.notificacion.id_noti);
      let data=[];
      data.push(this.notificacion.id_noti);
      localStorage.setItem('id_nots', JSON.stringify(data));
    }
  }
  
}
