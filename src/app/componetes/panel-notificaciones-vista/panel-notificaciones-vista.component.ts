import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';

@Component({
  selector: 'app-panel-notificaciones-vista',
  templateUrl: './panel-notificaciones-vista.component.html',
  styleUrls: ['./panel-notificaciones-vista.component.scss']
})
export class PanelNotificacionesVistaComponent implements OnInit {

  public allNotificaciones;
  constructor(
    private router:Router,
    private notificaionesService: NotificacionService
  ) { }

  ngOnInit(): void {
    this.allNotificaciones = JSON.parse(localStorage.getItem("info2"));  
  }

}
