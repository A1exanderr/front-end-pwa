import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelNotificacionesVistaComponent } from './panel-notificaciones-vista.component';

describe('PanelNotificacionesVistaComponent', () => {
  let component: PanelNotificacionesVistaComponent;
  let fixture: ComponentFixture<PanelNotificacionesVistaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelNotificacionesVistaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PanelNotificacionesVistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
