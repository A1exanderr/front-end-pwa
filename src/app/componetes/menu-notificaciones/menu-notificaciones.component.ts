import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';
import decode from 'jwt-decode';
import { interval } from 'rxjs';


@Component({
  selector: 'app-menu-notificaciones',
  templateUrl: './menu-notificaciones.component.html',
  styleUrls: ['./menu-notificaciones.component.scss']
})
export class MenuNotificacionesComponent implements OnInit {
  
  public user;

  constructor(
    private router:Router,
    private notificaionesService: NotificacionService
    ) {
      this.notificaionesService.solicitar_notifi();
      this.user = decode(localStorage.getItem('token'));
      this.notificaionesService.verificar_datos();
      const obs$ = interval(60*1000);
      obs$.subscribe( (d) => {
        this.notificaionesService.actualizar();
      });
      const tiempo$ = interval(30*1000);
      tiempo$.subscribe( (a) => {
        let data:any[];
        data = JSON.parse(localStorage.getItem("info1"));
        if(data.length > 0)
        {
          this.enviar_notificaciones();
        }
      });
    }

  ngOnInit(): void {
  }
  salir()
  {
    //localStorage.setItem('token', '');
    localStorage.clear();
    this.router.navigate(['login']);
  }
  actualizar()
  {
    //this.notificaionesService.cargar_notificaciones_no_vistas();
    //this.notificaionesService.cargar_notificaciones_vistas();
    this.notificaionesService.actualizar();
  }
  enviar_notificaciones()
  {
    this.notificaionesService.enviar_notigicacion();
    this.notificaionesService.enviar_sonido();
  }


}
