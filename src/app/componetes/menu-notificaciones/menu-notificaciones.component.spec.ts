import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuNotificacionesComponent } from './menu-notificaciones.component';

describe('MenuNotificacionesComponent', () => {
  let component: MenuNotificacionesComponent;
  let fixture: ComponentFixture<MenuNotificacionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuNotificacionesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MenuNotificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
