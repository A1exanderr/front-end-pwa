import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  formularioLogin:FormGroup;

  constructor(
    public formulario:FormBuilder,
    private router:Router,
    private authService: AuthService
  ) 
  {
    this.formularioLogin = this.formulario.group( {
      usuario:[''],
      password:['']
    } );
    console.log("HOLA ESTAS VERIFICANDO LA RED");
    if(localStorage.getItem('token'))
    {
      this.router.navigate(['panel_notificaciones']);
    }
  }
  

  ngOnInit(): void {
  }

  logIn(){
    //recuperar valor del formulario
    //console.log(this.formularioLogin.get('userName').value);
    //console.log(this.formularioLogin.get('pass').value);
    //console.log(this.user);
    //console.log(this.formularioLogin.value);
    this.authService.singin(this.formularioLogin.value).subscribe( (res:any) => {
      //console.log(res);
      if(res != 'Usuario o clave incorrectos')
      {
        localStorage.setItem('token', res.token);
        this.router.navigate(['panel_notificaciones']);
      }
      else{
        console.log(res);
      }
      
    })

  }

}
