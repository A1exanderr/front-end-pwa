import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificacionService } from 'src/app/services/notificacion.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-panel-notificaciones',
  templateUrl: './panel-notificaciones.component.html',
  styleUrls: ['./panel-notificaciones.component.scss']
})
export class PanelNotificacionesComponent implements OnInit {

  public allNotificaciones;

  constructor(
    private router:Router,
    private notificaionesService: NotificacionService
  ) { }

  ngOnInit(): void {
    this.allNotificaciones = JSON.parse(localStorage.getItem("info1"));  
  }
}
