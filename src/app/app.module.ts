import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//importaciones extras
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http' ;
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
//fin importaciones extras
//material icon
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
//fin de materil icon
//importar jwt
import { JwtHelperService, JWT_OPTIONS } from '@auth0/angular-jwt';
//fin de jwt
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LoginComponent } from './componetes/login/login.component';
import { MenuNotificacionesComponent } from './componetes/menu-notificaciones/menu-notificaciones.component';
import { PanelInformativoComponent } from './componetes/panel-informativo/panel-informativo.component';
import { PanelNotificacionesComponent } from './componetes/panel-notificaciones/panel-notificaciones.component';
import { PanelNotificacionesVistaComponent } from './componetes/panel-notificaciones-vista/panel-notificaciones-vista.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuNotificacionesComponent,
    PanelInformativoComponent,
    PanelNotificacionesComponent,
    PanelNotificacionesVistaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ScrollingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [
    { provide: JWT_OPTIONS, useValue: JWT_OPTIONS },
    JwtHelperService
  ],
  bootstrap: [AppComponent],
  exports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule
  ]
})
export class AppModule { }
