import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { menu- } from './componentes/menu-notificaciones';
import { AuthGuard } from './guards/auth.guard';
//componentes
import { LoginComponent } from './componetes/login/login.component';
import { MenuNotificacionesComponent } from './componetes/menu-notificaciones/menu-notificaciones.component';
import { PanelInformativoComponent } from './componetes/panel-informativo/panel-informativo.component';
import { PanelNotificacionesComponent } from './componetes/panel-notificaciones/panel-notificaciones.component';
import { PanelNotificacionesVistaComponent } from './componetes/panel-notificaciones-vista/panel-notificaciones-vista.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'menu_notificaciones', component: MenuNotificacionesComponent, canActivate: [AuthGuard] },
  { path: 'panel_informativo/:id', component: PanelInformativoComponent, canActivate: [AuthGuard] },
  { path: 'panel_notificaciones', component: PanelNotificacionesComponent, canActivate: [AuthGuard] },
  { path: 'panel_notificaciones_vistas', component: PanelNotificacionesVistaComponent, canActivate: [AuthGuard] }
];

@NgModule({
  //imports: [RouterModule.forRoot(routes)],
  imports: [RouterModule.forRoot(routes,{ useHash: true})],
  exports: [RouterModule]
})

export class AppRoutingModule { }
