import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//imports
import { interval } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import decode from 'jwt-decode';


@Injectable({
  providedIn: 'root'
})
export class NotificacionService {

  private URL = 'https://api.makerdev.tk';
  private user;

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService
  ) { }

  ///////////////////////////////
  ver_notificacion(id_noti:any)
  {
    //console.log(id_noti);
    return this.http.post(`${this.URL}/notificaciones/notificaciones_user`,id_noti);
  }
  ////CARGAR DESDE API//// 
  user_notificacion()
  {
    const token = localStorage.getItem('token');
    return this.http.post(`${this.URL}/notificaciones/noti_user`,decode(token));
  }
  user_notificacion_vistas()
  {
    const token = localStorage.getItem('token');
    return this.http.post(`${this.URL}/notificaciones/noti_vista_user`,decode(token));
  }
  notifi_vista_actualizar(id_noti:any)
  {
    console.log(id_noti);
    return this.http.post(`${this.URL}/notificaciones/noti_vis`,id_noti);
  }
  not_no_vis_actualizar()
  {
    let id_ultimo = Number(localStorage.getItem("ultimo"));
    this.user = decode(localStorage.getItem('token'));
    let data = {
      "id_user": this.user.id_user,
      "id_noti": id_ultimo
    };
    return this.http.post(`${this.URL}/notificaciones/noti_user_actualizar`,data);
  }
  ////CARGAR NOTIFICACIONES///
  cargar_notificaciones_no_vistas()
  {
    console.log("Sacando la informcion de apies");
    this.user_notificacion().subscribe(respuesta=>{
      localStorage.setItem('info1', JSON.stringify(respuesta));
      let data:any[];
      data = JSON.parse(localStorage.getItem("info1"));
      localStorage.setItem('ultimo', JSON.stringify(data[0].id_noti));
    });
  }
  cargar_notificaciones_vistas()
  {
    console.log("Sacando la informcion de apies");
    this.user_notificacion_vistas().subscribe(respuesta=>{
      console.log(respuesta);
      if(respuesta != "no tienes notificaciones")
      {
        localStorage.setItem('info2', JSON.stringify(respuesta));
      }
      else{
        let obj = [];
        localStorage.setItem('info2', JSON.stringify(obj));
      }
    });
  }
  ////ACTUALIZAR/////
  actualizar()
  {
    if(navigator.onLine)
    {
      console.log("SI TIENE CONEXION A INTERNE");
      this.cargar_datos_n();
    }
    else{
      console.log("NO TIENES CONEXION A INTERNET");
    }
  }
  cargar_datos_n()
  {
    let verificar = localStorage.getItem("id_nots");
    if(verificar != null)
    {
      let id_nots = JSON.parse(verificar);
      if(id_nots.length>0)
      {
        console.log(">>>",id_nots[id_nots.length-1]);
        let data = {
          'id_noti': id_nots[id_nots.length-1]
        };
        console.log(data);
        this.notifi_vista_actualizar(data).subscribe( (res:any) => {
          console.log(res);
          if(res == 'Se actulizo con exito')
          {
            //console.log(">>>>>", i);
            var eliminado = id_nots.filter((item) => item !== id_nots[id_nots.length-1]);
            localStorage.setItem('id_nots', JSON.stringify(eliminado));
            this.cargar_datos_n();
          }
        });
      }
      else{
        //console.log("HELOO POR NO",id_nots.length);
        console.log(">>>>>>>>HOLA ESTAS POR CARGAR LAS NUEVAS ACUALIZACIONES");
        this.not_no_vis_actualizar().subscribe(respuesta=>{
          if(respuesta != 'no tienes notificaciones')
          {
            let aux:any[];
            localStorage.setItem('aux', JSON.stringify(respuesta));
            localStorage.setItem('ultimo', JSON.stringify(respuesta[0].id_noti));
            let verNotificacion = JSON.parse(localStorage.getItem("info1"));
            aux = JSON.parse(localStorage.getItem("aux"));
            for(let i = aux.length-1; i >=0; i--)
            {
              let noti = aux.find(item => item.id_noti === aux[i].id_noti);
              verNotificacion.unshift(noti);
            }
            localStorage.setItem('info1', JSON.stringify(verNotificacion));
            const obs$ = interval(2000);
            obs$.subscribe( (d) => {
              //this.noti();
              console.log("ACTUALIZANDO LOS DATOS A LA NUBE");
              location.reload();
            });
          }
          else{
            console.log(respuesta);
          }
        });
      }
      
      
    }
    else{
      console.log("Ninguna informacion para actualizar");
    }
  }
  /*
  cargar_datos_n()
  {
    let verificar = localStorage.getItem("id_nots");
    if(verificar != null)
    {
      let id_nots = JSON.parse(verificar);
      if(id_nots.length>0)
      {
        for(let i = 0; i < id_nots.length; i++)
        {
          let data = {
            id_noti: id_nots[i]
          };
          this.notifi_vista_actualizar(data).subscribe( (res:any) => {
            console.log(res);
            if(res == 'Se actulizo con exito')
            {
              //console.log(">>>>>", i);
              var eliminado = id_nots.filter((item) => item !== id_nots[i]);
              localStorage.setItem('id_nots', JSON.stringify(eliminado));
              //this.cargar_datos_n();
            }
          });
        }
      }
      else{
        //console.log("HELOO POR NO",id_nots.length);
        console.log(">>>>>>>>HOLA ESTAS POR CARGAR LAS NUEVAS ACUALIZACIONES");
        this.not_no_vis_actualizar().subscribe(respuesta=>{
          if(respuesta != 'no tienes notificaciones')
          {
            let aux:any[];
            localStorage.setItem('aux', JSON.stringify(respuesta));
            localStorage.setItem('ultimo', JSON.stringify(respuesta[0].id_noti));
            let verNotificacion = JSON.parse(localStorage.getItem("info1"));
            aux = JSON.parse(localStorage.getItem("aux"));
            for(let i = aux.length-1; i >=0; i--)
            {
              let noti = aux.find(item => item.id_noti === aux[i].id_noti);
              verNotificacion.unshift(noti);
            }
            localStorage.setItem('info1', JSON.stringify(verNotificacion));
            const obs$ = interval(2000);
            obs$.subscribe( (d) => {
              //this.noti();
              console.log("ACTUALIZANDO LOS DATOS A LA NUBE");
              location.reload();
            });
          }
          else{
            console.log(respuesta);
          }
        });
      }
      
      
    }
    else{
      console.log("Ninguna informacion para actualizar");
    }
  }*/
  verificar_datos()
  {
    if((JSON.parse(localStorage.getItem("info2")) == null) && (JSON.parse(localStorage.getItem("info1")) == null))
    {
      this.cargar_notificaciones_vistas();
      this.cargar_notificaciones_no_vistas();
      const obs$ = interval(2000);
      obs$.subscribe( (d) => {
        //this.noti();
        console.log("cargar los datos en la pantalla por primera vez");
        location.reload();
      });
      //location.reload();
    }
  }
  //Eliminar notificaciones
  eliminar_notificacion(id:any, verNotificacion:any)
  {
    var eliminado = verNotificacion.filter((item) => item.id_noti != id);
    localStorage.setItem('info1', JSON.stringify(eliminado));
  }
  //verificar tipo de datos
  //console.log('-**-->',typeof id_nots,id_nots);
  //datos para la actualizacion.
  set_notificacion_vista(notificacion:any)
  {
    let verificar = localStorage.getItem("id_nots");
    if(verificar != null)
    {
      let id_nots = JSON.parse(verificar);
      id_nots.push(notificacion.id_noti);
      localStorage.setItem('id_nots', JSON.stringify(id_nots));
    }
    else{
      let data=[];
      data.push(notificacion.id_noti);
      localStorage.setItem('id_nots', JSON.stringify(data));
    }
  }
  //////////////////////////////////////////////////////////////
  ObtenerNotificacion(){
    if (window.Notification && Notification.permission === "granted") {
      var i = 0;
      // El uso de un intervalo hace que algunos navegadores (incluido Firefox)
      // bloqueen las notificaciones si hay demasiadas en un tiempo determinado.
      var interval = window.setInterval(function () {
        // Gracias a la etiqueta, solo deberíamos ver la notificación "¡Hola! 9"
        var n = new Notification("¡Hola soy Alex! " + i, {tag: 'soManyNotification'});
        if (i++ == 9) {
          window.clearInterval(interval);
        }
      }, 200);
    }
    else if (window.Notification && Notification.permission !== "denied") {
      Notification.requestPermission(function (status) {
        // Si el usuario dijo que está bien
        if (status === "granted") {
          var i = 0;
          // El uso de un intervalo hace que algunos navegadores (incluido
          // Firefox) bloqueen las notificaciones si hay demasiadas en un tiempo
          // determinado.
          var interval = window.setInterval(function () {
            // Gracias a la etiqueta, solo deberíamos ver la notificación "¡Hola! 9"
            var n = new Notification("¡Hola denegaste la notificacion! " + i, {tag: 'soManyNotification'});
            if (i++ == 9) {
              window.clearInterval(interval);
            }
          }, 200);
        }

        // De lo contrario, podemos recurrir a una alerta modal regular.
        else {
          alert("¡Hola!");
        }
      });
    }
    else {
      // Podemos recurrir a una alerta modal regular
      alert("¡Hola soy sander esto es cuando estas con ip sin utilizar localhost!");
    }
  }

  solicitar_notifi()
  {
    Notification.requestPermission().then(function (permission) {
      console.log(permission);
    });
  }

  enviar_notigicacion()
  {
    (async () => {
      // create and show the notification
      const showNotification = () => {
          // create a new notification
          const notification = new Notification('NOTIFICACIONES DEL SISTEMA', {
              body: 'TOTAL DE NOTIFICACIONES DISPONIBLES QUE TIENES SON:',
              //icon: 'https://cdn-icons-png.flaticon.com/512/1301/1301416.png'
              icon: 'assets/icons/icon-72x72.png'
              //vibrate: true
          });

          // close the notification after 10 seconds
          setTimeout(() => {
              notification.close();
          }, 10 * 1000);

          // navigate to a URL when clicked
          notification.addEventListener('click', () => {

              window.open('https://tokapu.notificaciones.tk/', '_blank');
          });
      }

      // show an error message
      const showError = () => {
        const error = document.querySelector('.error');
        alert("¡Hola soy sander esto es cuando estas con ip sin utilizar localhost!");
      }

      // check notification permission
      let granted = false;

      if (Notification.permission === 'granted') {
          granted = true;
      } else if (Notification.permission !== 'denied') {
          let permission = await Notification.requestPermission();
          granted = permission === 'granted' ? true : false;
      }

      // show notification or error
      granted ? showNotification() : showError();

    })();
  }

  enviar_sonido()
  {
    console.log("Ejecutando la funcion de sonido");
    let sound = new Audio('assets/sonido/sonido1.mp3');
    sound.play();
  }
  ////////////////////////////////////////////////////////////////////////////////////

}
