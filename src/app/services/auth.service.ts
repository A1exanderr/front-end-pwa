import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
//imports
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private URL = 'https://api.makerdev.tk';

  constructor(
    private http: HttpClient,
    private jwtHelper: JwtHelperService) 
  {
    
  }
  singin(user:any)
  {
    //return this.http.post('http://147.182.189.231:3000/user/singin',user);
    //console.log(user);
    return this.http.post(`${this.URL}/user/singin`,user);
  }
  isAuth():boolean
  {
    const token = localStorage.getItem('token');
    if( this.jwtHelper.isTokenExpired(token) || !localStorage.getItem('token'))
    {
      return false;
    }
    return true;
  }
}